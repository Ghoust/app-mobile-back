import h5py
import numpy as np

from .VGG import VGGNet

max_result = 5

def retrieve_similar_images(image):
    similar_images = []
    h5f = h5py.File("./mobile_back/featuresCNN.h5", 'r')
    features = h5f["dataset_features"][:]
    image_names = h5f["dataset_paths"][:]
    h5f.close()

    # Init VGGNet16 model
    model = VGGNet()

    # Extract query image's feature, compute similarity score and sort
    image_features = model.extract_features(image)
    scores = np.dot(image_features, features.T)
    rank_id = np.argsort(scores)[::-1]
    rank_score = scores[rank_id]

    imlist = [image_names[index] for i, index in enumerate(rank_id[0:max_result])]

    # show top retrieved result one by one
    for i, im in enumerate(imlist):
        t = (im, rank_score[i])
        similar_images.append(t)
    return similar_images
