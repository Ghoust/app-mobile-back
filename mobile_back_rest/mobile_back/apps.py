from django.apps import AppConfig


class MobileBackConfig(AppConfig):
    name = 'mobile_back'
