import os

from django.conf import settings
from rest_framework.views import APIView

from .serializers import RechercheSerializer, ResultatSerializer
from rest_framework.response import Response
from rest_framework import status

from .models import Recherche, Resultat
from .similarity import retrieve_similar_images


class ImageSearch(APIView):

    def post(self, request):
        """
        Uploads an image and insert it in the database, alongside the similar images that have benn found.
        """
        serializer = RechercheSerializer(data=request.data)

        if serializer.is_valid():
            instance = serializer.save()
            image_path = os.path.join(settings.MEDIA_ROOT, instance.image.file.name)
            result_tuple = retrieve_similar_images(image_path)
            for t in result_tuple:
                image = t[0].decode("utf8")
                score = t[1]
                resultat = Resultat(image=image, taux=score, id_recherche=instance)
                resultat.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, pk):
        """
        Returns all similar image with their similarity rate using the user image of id 'pk'
        """
        obj = Resultat.objects.filter(id_recherche=pk)
        results = ResultatSerializer(obj, many=True)
        return Response({"Results": results.data}, status=status.HTTP_200_OK)

    def delete(self, request):
        """
        Debugging function to erase of data from the database.
        """
        Recherche.objects.all().delete()
        Resultat.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
