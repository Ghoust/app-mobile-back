import os

import h5py
import numpy as np
import imghdr

from mobile_back_rest.mobile_back_rest.settings import MEDIA_ROOT
from mobile_back_rest.mobile_back.VGG import VGGNet


def retrieve_images_from_folder(folder: str) -> list:
    """
    Searching recursively for image-like files, starting from a directory passed as a parameter.
    Files that are not an image are not added the returned list
    @param folder:
    @return: List of all images
    """
    image_paths = list()
    folder = os.path.join(MEDIA_ROOT, folder)
    for (directory_path, directory_names, file_names) in os.walk(folder):
        for filename in file_names:
            path = os.path.join(directory_path, filename)
            if imghdr.what(path) is not None:
                path = path.split(MEDIA_ROOT)[1].strip('\\')
                image_paths.append(path)
    return image_paths


if __name__ == "__main__":
    images = retrieve_images_from_folder("base")
    features = []
    paths = []
    model = VGGNet()

    print("[INFO] Extracting features of %d images..." % len(images))
    for i, image_path in enumerate(images):
        if i % 100 == 0:
            print("[INFO] Extracting features of image #%d" % i)
        normalized_features = model.extract_features(MEDIA_ROOT + "\\" + image_path)
        features.append(normalized_features)
        paths.append(image_path)

    print("--------------------------------------------------")
    print("      writing feature extraction results ...")
    print("--------------------------------------------------")

    features = np.array(features)
    paths = np.array(paths, dtype='S')

    h5f = h5py.File("featuresCNN.h5", 'w')
    h5f.create_dataset('dataset_features', data=features)
    h5f.create_dataset('dataset_paths', data=paths)
    h5f.close()
