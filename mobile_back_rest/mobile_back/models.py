from django.db import models
from django.utils import timezone


class Recherche(models.Model):
    image = models.ImageField(upload_to='uploads/')

    @property
    def search_date(self):
        return timezone.now()


class Resultat(models.Model):
    image = models.ImageField()
    taux = models.FloatField()
    id_recherche = models.ForeignKey(Recherche, on_delete=models.CASCADE)
